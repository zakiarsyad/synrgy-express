const express = require('express')
const path = require('path')

const router = require('./routes/router')

const app = express()

app.use(express.json())
app.use(express.urlencoded())
app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'ejs')

app.use('/', router)

app.use((err, req, res, next) => {
  if (err) {
    console.log(err)
  }
})

app.listen(3000, () => {
  console.log(`Example app listening at http://localhost:3000`)
})

module.exports = app