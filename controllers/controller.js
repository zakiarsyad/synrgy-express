class Controller {
    static users = []

    static getRegisterPage(req, res) {
        res.render('register')
    }

    static registerNewUser(req, res) {
        const { username, password } = req.body

        // logic to register a new user
        Controller.users.push({
            username: username,
            password: password
        })

        console.log(Controller.users)

        res.render('home', {
            name: username
        })
    }

    static logout(req, res) {
        res.render('logout')
    }

    static getLoginPage(req, res) {
        res.render('login')
    }

    static login(req, res) {
        const { username, password } = req.body

        // check username & password
        console.log(Controller.users)
        console.log(username)
        console.log(password)

        if (!username) {
            res.render('login')
        }

        res.render('home', {
            name: username
        })
    }
}

module.exports = Controller