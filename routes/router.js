const express = require('express')
const router = express.Router()

const controller = require('../controllers/controller')

const logger = require('../middlewares/logger')

router.use(logger)

router.get('/register', controller.getRegisterPage)
router.post('/register', controller.registerNewUser)
router.get('/logout', controller.logout)

router.get('/login', controller.getLoginPage)
router.post('/login', controller.login)

module.exports = router